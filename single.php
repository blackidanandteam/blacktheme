<?php
add_action('wp_head',function(){
    ?>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
    <?PHP
});
get_header(); 
?>
<div id="fb-root"></div>
<?PHP
while ( have_posts() ) : the_post(); ?>
<div class="pageHeader">
    <div class="wrapper">
        <h1><?PHP the_title(); ?></h1>
    </div>
</div><!-- EOF : Page Header -->
<div class="mainContent">
    <div class="wrapper">
        <div class="b_contentLeft">
        <div><?PHP the_content(); ?></div>
        <div class="fb-comments" data-href="<?PHP echo get_permalink(); ?>" data-width="650" data-num-posts="10"></div>
        <?php //comments_template( '', true ); ?>
        </div>
        <?PHP get_sidebar(); ?>        
        <div class="clear"></div>
    </div>
</div>
<?php 
        endwhile; // end of the loop. ?>
<?php get_footer(); ?>