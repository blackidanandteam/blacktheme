<?php
get_header(); 
while ( have_posts() ) : the_post(); ?>
<div class="pageHeader">
    <div class="wrapper">
        <h1><?PHP the_title(); ?></h1>
    </div>
</div><!-- EOF : Page Header -->
<div class="mainContent">
    <div class="wrapper">
        <div class="b_contentLeft">
        <div><?PHP the_content(); ?></div>
        </div>
        <?PHP get_sidebar(); ?>        
        <div class="clear"></div>
    </div>
</div>
<?php 
        endwhile; // end of the loop. ?>
<?php get_footer(); ?>