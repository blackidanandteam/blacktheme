<?php

get_header(); 
?>
<div class="pageHeader"></div>
<div class="mainContent">
    <div class="wrapper">
    <div class="s_pageContent">
    <div class="s_mainContent">
        <div class="inner_heading"><h2><?PHP _e('404 : Page not Found'      ,'BLACK_TEXTDOMAIN'); ?></h2></div>
        <div class="message">
            <div class="main_message"><?PHP echo _e("404 : Page Not Found"  ,'BLACK_TEXTDOMAIN'); ?></div>
            <div class="general_note">
            </div>
        </div>
    </div>
    </div>
    </div>
</div>
<?php 
get_footer();
?>