<?php
/**
 * @package Black
 * @subpackage wp_post
 * @author Anand Thakkar <anand@blackid.com>
 */

/**
 * Get Postmeta Details In more Optimized Way Based On post_id And meta_key
 * 
 * @param $post_id int|Array
 * @param $meta_key string|Array
 * @uses $wpdb
 * @return Array
 */
function black_get_postmeta_detail($post_id,$meta_key=NULL){
    global $wpdb;
    if(is_array($post_id)){
        if(is_null($meta_key)){
            $qry="SELECT * FROM $wpdb->postmeta WHERE post_id IN (" . implode(",",$post_id) . ")";
        }elseif(is_array($meta_key)){
            for($i=0;$i<count($meta_key);$i++)
                $meta_key[$i]="'" . $meta_key[$i] . "'";
            $qry="SELECT * FROM $wpdb->postmeta WHERE post_id IN (" . implode(",",$post_id) . ") AND meta_key IN (" . implode(",",$meta_key) . ")";
        }else
            $qry="SELECT * FROM $wpdb->postmeta WHERE post_id IN (" . implode(",",$post_id) . ") AND meta_key='$meta_key'";
        
        $result=$wpdb->get_results($qry,ARRAY_A);
        if(count($result)>0) {
            for($i=0;$i<count($result);$i++)
                $post_meta[$result[$i]['post_id']][$result[$i]['meta_key']]=$result[$i]['meta_value'];
            return $post_meta;
        }else
            return NULL;
    }else{
        
        if(is_null($meta_key)){
            $qry="SELECT * FROM $wpdb->postmeta WHERE post_id='$post_id'";
        }elseif(is_array($meta_key)){
            for($i=0;$i<count($meta_key);$i++)
                $meta_key[$i]="'" . $meta_key[$i] . "'";
            $qry="SELECT * FROM $wpdb->postmeta WHERE post_id='$post_id' AND meta_key IN (" . implode(",",$meta_key) . ")";
        }else
            return get_post_meta($post_id, $meta_key);
        
        $result=$wpdb->get_results($qry,ARRAY_A);
        if(count($result)>0){
            for($i=0;$i<count($result);$i++)
                $post_meta[$result[$i]['meta_key']]=$result[$i]['meta_value'];
            return $post_meta;
        }else
            return NULL;
    }
}
/**
 * 
 */
function black_delete_user_metalike($meta_key,$user_id = NULL ){
    global $wpdb;
    
    $qry['A']  = " DELETE FROM {$wpdb->usermeta} WHERE ";
    $qry['A'] .= " meta_key LIKE '%{$meta_key}%' ";
    if(!is_null($user_id)){
        $qry['A'] .= " AND user_id={$user_id}";
    }
    
    return $wpdb->query($qry['A']);

}