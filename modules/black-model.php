<?php

class BLACK_MODEL 
{
    public $page_no;
    public $per_page;
    public $order_by;
    public $order;
    public $table_name;
    public $result_type;
    public $primary_key;

    private $qry;        
    private $result;     
    private $wpdb;       

    function __construct($args = array()) {
        global $wpdb;
        
        $this->qry      = array();
        $this->result   = array();
        $this->wpdb     = $wpdb;
        
        $this->table_name       = $this->wpdb->prefix . $args['table_name'];
        
        $this->init($args);
    }
    
    /**
     * Initalize the Query Variable 
     * 
     * @param array|NULL $args
     */
    function init($args = array()){
        
        $this->per_page         = isset($args['per_page'])?$args['per_page']:10;
        $this->page_no          = isset($args['page_no']) ?$args['page_no'] :1;
        $this->order_by         = isset($args['order_by'])?$args['order_by']:'';
        $this->order            = (strtolower($args['order'])=='asc')?'ASC':'DESC';
        $this->result_type      = (isset($args['result_type']))?$args['result_type']:'';
        $this->calculate_total  = (isset($args['calculate_total']))?TRUE:FALSE;
    }
    
    /**
     * Get the total Count 
     * 
     * @param int $args
     */
    function get_total($args){
        
    }
    
    /**
     * 
     * @param type $args
     * @return type
     */
    function get_all($args = array()){
        
        $this->init();
        
        $this->qry['A'] = " SELECT * FROM {$this->table_name} ";
        
        return $this->wpdb->get_results($this->qry['A'],$this->result_type);
    }
    
    
    
     
}
