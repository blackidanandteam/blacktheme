<?php
/**
 * @package Black
 * @subpackage E-mail
 */

/**
 * Store all the E-mail of the System And their Var Details 
 */
global $black_email;
global $black_email_type;

function black_email_init(){
    
    register_post_type( 'black_email',
            array(
                'labels' => array(
                        'name'               => __('E-mails'                   , BLACK_TEXTDOMAIN),
                        'singular_name'      => __('E-mail'                    , BLACK_TEXTDOMAIN),
                        'add_new'            => __('Add E-mail'                , BLACK_TEXTDOMAIN),
                        'all_items'          => __('E-mails'                   , BLACK_TEXTDOMAIN),
                        'add_new_item'       => __('Add New E-mail'            , BLACK_TEXTDOMAIN),
                        'edit_item'          => __('Edit E-mail'               , BLACK_TEXTDOMAIN),
                        'new_item'           => __('New E-mail'                , BLACK_TEXTDOMAIN),
                        'view_item'          => __('View E-mail'               , BLACK_TEXTDOMAIN),
                        'search_items'       => __('Search E-mails'            , BLACK_TEXTDOMAIN),
                        'not_found'          => __('No E-mails found'          , BLACK_TEXTDOMAIN),
                        'not_found_in_trash' => __('No E-mails found in Trash' , BLACK_TEXTDOMAIN),
                        ),
                'show_in_menu'       => true,
                'public'             => false,
                'publicly_queryable' => true,
                'has_archive'        => false,
                'hierarchical'       => true,
                'show_ui'            => true,
                'rewrite'            => array('slug'=>'black_email','with_front' => false),
                'supports'           => array('title','editor'),
            )
    );
}

add_action('init','black_email_init');

function black_email_admin_menu(){
        
    $page=add_submenu_page(
            'edit.php?post_type=black_email',
            __('Settings',BLACK_TEXTDOMAIN),
            __('Settings',BLACK_TEXTDOMAIN),
            'manage_options', 'black_email_settings', 'black_email_settings'
    );
}

add_action('admin_menu','black_email_admin_menu');

if(isset($_POST['black_do_action']) && current_user_can('manage_options') && $_POST['black_do_action']=='black_email_update_settings'){
    
    $value = array(
        'mail_body'     => $_REQUEST['black_email_body'],
        'from'          => $_REQUEST['black_email_from'],
        'username'      => $_REQUEST['black_email_username'],
        'password'      => $_REQUEST['black_email_password'],
        'headers'       => $_REQUEST['black_email_headers'],
        'host'          => $_REQUEST['black_email_host'],
        'content_type'  => $_REQUEST['black_email_content_type'],
    );
    
    update_option('black_email_settings', $value);
    
    wp_redirect(admin_url('edit.php?post_type=black_email&page=black_email_settings&message=option_updated'));
}

function black_email_prepare_mail($data,$template){
    $mail_obj = array();
    
    foreach($template['vars'] as $key=>$value){
        $pattern[]     = "#\%$key\%#";
        $replacement[] = $data[$value['class']][$value['property']];
    }
    
    $body                       = preg_replace($pattern,$replacement, $template['post_content']);
    
    $mail_obj['to']             = $data['to'];
    $mail_obj['to_email']       = $data['to_email'];
    $mail_obj['type']           = $template['black_email_type'];
    $mail_obj['from']           = $template['black_email_from'];
    $mail_obj['username']       = $template['black_email_username'];
    $mail_obj['password']       = $template['black_email_password'];
    $mail_obj['host']           = $template['black_email_host'];
    $mail_obj['content_type']   = $template['black_email_content_type'];
    $mail_obj['subject']        = preg_replace($pattern,$replacement, $template['black_email_subject']);
    $mail_obj['body']           = apply_filters('the_content',preg_replace('#\%body\%#',$body, stripslashes($template['mail_body'])));
    
    
    return $mail_obj;
    
    
}

function black_send_mail($mail_obj){
    switch($mail_obj['type']){
        case 'phpmail':
        default:  
            
            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type:' . $mail_obj['content_type'] . "\r\n";

            // Mail it
            mail($mail_obj['to_email'], $mail_obj['subject'], $mail_obj['body'], $headers);
            break;
        case 'smtpmail':    
            include_once 'Mail.php';
            
            $headers = array(
                'From'              => $mail_obj['from'],
                'To'                => "{$mail_obj['to']} <{$mail_obj['to_mail']}>",
                'Subject'           => $mail_obj['subject'],
                'Content-Type'      => $mail_obj['content_type'], 
            );
            
            $smtp = Mail::factory('smtp',array(
                'host'       => $mail_obj['host'],
                'auth'       => true,
                'port'       => 587,
                'username'   => $mail_obj['username'],
                'password'   => $mail_obj['password'],
            ));
            
            $mail = $smtp->send($mail_obj['to'],$headers,$mail_obj['body']);
            
            if (PEAR::isError($mail)) {
                var_dump($mail);
                return FALSE;
            }else {
                return TRUE;
            }
            
            break;
    }
}

function black_email_settings(){
    
    if(isset($_GET['message']))
        $message = $_GET['message'];
    else
        $message = '';
    
    $black_email_settings = get_option('black_email_settings',array(
        'mail_body'         => '',
        'from'              => '',
        'username'          => '',
        'password'          => '',
        'headers'           => '',
        'host'              => '',
        'content_type'      => '',
    ));
    
    ?>
    <div class="wrap">
        <div id="icon-options-general" class="icon32"><br></div>
        <h2><?PHP echo __('E-mail Settings',BLACK_TEXTDOMAIN); ?></h2>
        <?PHP
        switch($message){
            case 'option_updated':
                ?>
        <div class="updated fade"><p><?PHP echo __('Option Value(s) Updated Succesfully',BLACK_TEXTDOMAIN); ?></p></div>
                <?PHP
                break;
        }
        ?>
        <form action="edit.php?post_type=black_email&page=black_email_settings" method="POST">
        <input type="hidden" name="black_do_action" value="black_email_update_settings" />
        <h3><?PHP echo __("E-mail Body Template",BLACK_TEXTDOMAIN); ?></h3>
        <?PHP the_editor(stripslashes($black_email_settings['mail_body']),'black_email_body', '', FALSE); ?>
        <h3><?PHP echo __("General Options",BLACK_TEXTDOMAIN); ?></h3>
        <table class="form-table">
            <tbody>
                <tr valign="top">
                    <td scope="row"><label><?PHP echo __('From',BLACK_TEXTDOMAIN); ?></label></td>
                    <td>
                        <input type="text" name="black_email_from"
                               value="<?PHP echo $black_email_settings['from']; ?>"
                               class="regular-text" />
                    </td>
                </tr>
                <tr valign="top">
                    <td scope="row"><label><?PHP echo __('Username',BLACK_TEXTDOMAIN); ?></label></td>
                    <td>
                        <input type="text" name="black_email_username"
                               value="<?PHP echo $black_email_settings['username'];?>"
                               class="regular-text" />
                    </td>
                </tr>
                <tr valign="top">
                    <td scope="row"><label><?PHP echo __('Password',BLACK_TEXTDOMAIN); ?></label></td>
                    <td>
                        <input type="text" name="black_email_password"
                               value="<?PHP echo $black_email_settings['password']; ?>"
                               class="regular-text" />
                    </td>
                </tr>
                <tr valign="top">
                    <td><label><?PHP echo __('Host',BLACK_TEXTDOMAIN); ?></label></td>
                    <td>
                        <input type="text" name="black_email_host"
                               value="<?PHP echo $black_email_settings['host'];?>"
                               class="regular-text" />
                    </td>
                </tr>
                <tr valign="top">
                    <td><label><?PHP echo __('Content Type',BLACK_TEXTDOMAIN); ?></label></td>
                    <td>
                        <input type="text" name="black_email_content_type"
                               value="<?PHP echo $black_email_settings['content_type'];?>"
                               class="regular-text" />
                    </td>
                </tr>
                <tr valign="top">
                    <td scope="row"><label><?PHP echo __('Headers',BLACK_TEXTDOMAIN); ?></label></td>
                    <td>
                        <textarea name = "black_email_headers" id="black_email_headers"
                                  class= "large-text code" ><?PHP echo $black_email_settings['headers']; ?></textarea>
                    </td>
                </tr>        
            </tbody>
        </table>
        <div class="submit">
            <input type="submit"             value="<?PHP echo __('Save Settings',BLACK_TEXTDOMAIN); ?>"            
                   class="button-primary"    name="black_email_save_settings" />
        </div>
        </form>
    </div>
    <?PHP
}
    
    
function black_add_email($action_key,$action_name,$vars){
    global $black_email;
    
    $black_email[$action_key] = array(
        'name' => $action_name,
        'var'  => $vars
     );
}

function black_add_email_type($email_key,$args){
    global $black_email_type;
    
    $black_email_type[$email_key] = array(
        'name' => $args['name'],
     );
}

add_action( 'add_meta_boxes', 'black_email_add_meta_boxes' );

function black_email_add_meta_boxes(){
    add_meta_box('black_email_extra_field'  ,__( 'E-mail Configrations', BLACK_TEXTDOMAIN ),
                 'black_email_extra_field'  ,'black_email');
    
    add_meta_box('black_email_extra_vars'   ,__( 'E-mail Variables', BLACK_TEXTDOMAIN ),
                 'black_email_extra_vars'   ,'black_email','side');
}

function black_email_extra_vars(){
    global $black_email;
    $post_id = $_GET['post'];
    if(empty($post_id)){
        $post_id = -1;
    }
    ?>
    <label>
        <?PHP _e('Select E-mail Template' , BLACK_TEXTDOMAIN); ?>
        <select name="black_email_template" id="black_email_template">
            <option value="NS">--- <?PHP _e('Select E-mail Template',BLACK_TEXTDOMAIN); ?> ---</option>
            <?PHP
            if(is_array($black_email)){
                foreach($black_email as $key=>$value){
                    if($key == get_post_meta($post_id,'black_email_template', TRUE))
                        echo "<option selected=\"selected\" value=\"{$key}\">{$value['name']}</option>";
                    else
                        echo "<option value=\"{$key}\">{$value['name']}</option>";
                }
            }
            ?>
        </select>
    </label>
    <div id="black_email_response_text"></div>
    <style type="text/css">
        dt {font-weight:bold;margin:5px;  }
        dl {cursor:pointer}
    </style>
    <script type="text/javascript">
        var black_email = <?PHP echo json_encode($black_email); ?>;
        
        jQuery(document).ready(function(){
            jQuery('#black_email_template').change(function(){
                var vars = eval('black_email.' + jQuery(this).val() + '.var');
                var txt  = '<dl>';
                jQuery.each(vars,function(key,value){
                    txt += '<dt>%' + key + '%</dt>';
                    txt += '<dd>' + value.desc + '</dd>';
                });
                txt += '</dl>';
                jQuery('#black_email_response_text').html(txt);
            });
            
            jQuery('body').delegate('dt','click',function(){
                tinyMCE.activeEditor.execCommand('mceInsertContent', false, jQuery(this).html().trim());
            });
            
            if(jQuery('#black_email_template').val()!='NS')
                jQuery('#black_email_template').trigger('change');
        })
    </script>
    <?PHP
}

add_action('save_post','black_email_save_post');

function black_email_save_post($post_id){
    global $black_email,$black_email_type;
    $post = get_post($post_id);
    if($post->post_type == 'black_email'){
        
        if(in_array($_REQUEST['black_email_template'],array_keys($black_email)))
            update_post_meta($post_id  ,'black_email_template'   ,strip_tags($_REQUEST['black_email_template']));
        else
            delete_post_meta($post_id  ,'black_email_template');
        
        if(in_array($_REQUEST['black_email_type'], array_keys($black_email_type)))
            update_post_meta($post_id  ,'black_email_type'       ,strip_tags($_REQUEST['black_email_type']));
        else
            delete_post_meta ($post_id ,'black_email_type');
        
        update_post_meta($post_id  ,'black_email_subject'        ,strip_tags($_REQUEST['black_email_subject']));
        update_post_meta($post_id  ,'black_email_to'             ,strip_tags($_REQUEST['black_email_to']));
        update_post_meta($post_id  ,'black_email_from'           ,strip_tags($_REQUEST['black_email_from']));
        update_post_meta($post_id  ,'black_email_username'       ,strip_tags($_REQUEST['black_email_username']));
        update_post_meta($post_id  ,'black_email_password'       ,strip_tags($_REQUEST['black_email_password']));
        update_post_meta($post_id  ,'black_email_host'           ,strip_tags($_REQUEST['black_email_host']));
        update_post_meta($post_id  ,'black_email_content_type'   ,strip_tags($_REQUEST['black_email_content_type']));
        update_post_meta($post_id  ,'black_email_headers'        ,strip_tags($_REQUEST['black_email_headers']));
        
    }
}

function black_email_get_template($template_name){
    global $wpdb,$black_email;
    
    $qry  = " SELECT post_id FROM $wpdb->postmeta WHERE meta_key='black_email_template' ";
    $qry .= " AND meta_value='{$template_name}' ";
    
    
    $result = $wpdb->get_row($qry);
    if(is_null($result))
        return NULL;
    
    $post      = (array)get_post($result->post_id);
    $post_meta = black_get_postmeta_detail($result->post_id);
    $settings  = get_option('black_email_settings');
    $data      = array_merge($post,$post_meta,$settings);
    
    if(empty($data['black_email_from']))
        $data['black_email_from']           = $settings['from'];
    if(empty($data['black_email_username']))
        $data['black_email_username']       = $settings['username'];
    if(empty($data['black_email_password']))
        $data['black_email_password']       = $settings['password'];
    if(empty($data['black_email_headers']))
        $data['black_email_headers']        = $settings['headers'];
    if(empty($data['black_email_host']))
        $data['black_email_host']           = $settings['host'];
    if(empty($data['black_email_content_type']))
        $data['black_email_content_type']   = $settings['content_type'];
    
    $data['vars'] = $black_email[$template_name]['var'];
    
    return $data;
}

function black_email_extra_field(){
    global $black_email_type;
    $post_id = $_GET['post'];
    if(empty($post_id)){
        $post_id = -1;
    }
    ?>
    <table class="form-table">
        <tr>
            <td><label><?PHP echo __('E-mail Type',BLACK_TEXTDOMAIN); ?></label></td>
            <td>
                <select name="black_email_type" id="black_email_type">
                    <option value="NS">-- Select Mail Type --</option>
                <?PHP
                    foreach ($black_email_type as $key=>$val){
                        if($key==  get_post_meta($post_id, 'black_email_type', TRUE))
                            echo "<option selected='selected' value='{$key}'>{$val['name']}</option>";
                        else
                            echo "<option value='{$key}'>{$val['name']}</option>";
                    }
                ?>
                </select>
            </td>
        </tr>
        <tr>
            <td><label><?PHP echo __('Subject',BLACK_TEXTDOMAIN); ?></label></td>
            <td>
                <input type="text" name="black_email_subject"
                       value="<?PHP echo get_post_meta($post_id,'black_email_subject', TRUE);?>"
                       class="regular-text" />
            </td>
        </tr>
        <tr>
            <td><label><?PHP echo __('To',BLACK_TEXTDOMAIN); ?></label></td>
            <td>
                <input type="text" name="black_email_to"
                       value="<?PHP echo get_post_meta($post_id,'black_email_to', TRUE);?>"
                       class="regular-text" />
            </td>
        </tr>
        <tr>
            <td><label><?PHP echo __('From',BLACK_TEXTDOMAIN); ?></label></td>
            <td>
                <input type="text" name="black_email_from"
                       value="<?PHP echo get_post_meta($post_id,'black_email_from', TRUE);?>"
                       class="regular-text" />
            </td>
        </tr>
        <tr>
            <td><label><?PHP echo __('Username',BLACK_TEXTDOMAIN); ?></label></td>
            <td>
                <input type="text" name="black_email_username"
                       value="<?PHP echo get_post_meta($post_id,'black_email_username', TRUE);?>"
                       class="regular-text" />
            </td>
        </tr>
        <tr>
            <td><label><?PHP echo __('Password',BLACK_TEXTDOMAIN); ?></label></td>
            <td>
                <input type="text" name="black_email_password"
                       value="<?PHP echo get_post_meta($post_id,'black_email_password', TRUE);?>"
                       class="regular-text" />
            </td>
        </tr>
        <tr>
            <td><label><?PHP echo __('Host',BLACK_TEXTDOMAIN); ?></label></td>
            <td>
                <input type="text" name="black_email_host"
                       value="<?PHP echo get_post_meta($post_id,'black_email_host', TRUE);?>"
                       class="regular-text" />
            </td>
        </tr>
        <tr>
            <td><label><?PHP echo __('Content Type',BLACK_TEXTDOMAIN); ?></label></td>
            <td>
                <input type="text" name="black_email_content_type"
                       value="<?PHP echo get_post_meta($post_id,'black_email_content_type', TRUE);?>"
                       class="regular-text" />
            </td>
        </tr>
        <tr>
            <td><label><?PHP echo __('Headers',BLACK_TEXTDOMAIN); ?></label></td>
            <td>
                <textarea name = "black_email_headers" id="black_email_headers"
                          class= "large-text code" ><?PHP echo get_post_meta($post_id,'black_email_password', TRUE); ?></textarea>
            </td>
        </tr>
    </table>
    <?PHP
}

add_filter('contextual_help','black_email_add_help_tab',10,3);

/**
 * Add the Help Content for the User using context Api of the Scrren Which helps
 * User as help Manual 
 */
function black_email_add_help_tab ($contextual_help, $screen_id, $screen) {
    $screen = get_current_screen();
    
    if($screen_id!='edit-black_email' && $screen_id!='black_email' )
        return;
    
    $screen->add_help_tab( array(
        'id'            => 'black_email_overview',
        'title'         => __('Overview'),
        'content'	=> '<p>' . __( 'Descriptive content that will show in My Help Tab-body goes here.' . $screen_id ) . '</p>',
    ));
}

