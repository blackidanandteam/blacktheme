<?php

/**

 * @package Black
 * @subpackage Email_verification
 * @version 1.0
 */

/**
 * Is the Required detail to verfy the E-mail Is Provided Or not 
 * @since 1.0
 * @return boolean
 */
function black_email_has_verification_detail(){
    if(
            isset($_REQUEST['key'])      &&  !empty($_REQUEST['key'])
                                         && 
            isset($_REQUEST['username']) &&  !empty($_REQUEST['username'])
    ){
        return TRUE;
    }else
        return FALSE;
}

/**
 * Checkes Weather the Provided Verification details are correct or not 
 * 
 * @global type $wpdb
 * @return boolean
 * @since 1.0
 */
function black_email_verifyaccount(){
    global $wpdb;
    
    $key        = $_REQUEST['key'];
    $username   = $_REQUEST['username'];
    
    if(!black_email_has_verification_detail())
        return FALSE;
    
    if(($user_id = username_exists($username))){
        $qry  = " SELECT * FROM {$wpdb->users} WHERE ID={$user_id} ";
        $qry .= " AND user_activation_key ='{$key}'";
        
        $result = $wpdb->get_row($qry);
        
        if(count($result)>0){
            $wpdb->update(
                $wpdb->users,
                array(
                    'user_status'           => 0,
                    'user_activation_key'   => ''),
                array('ID'=>$user_id)
            );
            return TRUE;
        }else
            return FALSE;
    }else{
        return FALSE;
    }
}

/**
 * Generates the Verification Key for the User and store the Key in the Database
 * 
 * @global type $wpdb
 * @param int $user_id
 * @param int $key_length
 * @return array|null
 * @since 1.0
 */
function black_email_generate_verfication_key($user_id,$key_length = 20){
    global $wpdb;
    
    $random_key = wp_generate_password($key_length,FALSE);
    
    $wpdb->update(
            $wpdb->users,
            array(
                'user_status'        => 1,
                'user_activation_key'=> $random_key),
            array('ID'=>$user_id)
    );
    
    return array(
        'user_id' => $user_id,
        'key'     => $random_key,
    );
}