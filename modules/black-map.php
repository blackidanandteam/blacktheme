<?php
/**

 * @package Black
 * @subpackage Google_Map
 * @author Anand Thakkar <anand@blackid.com>
 * @version 1.0
 */

define('BLACK_GOOGLE_API','api');

/**
 * Perform the HTTP request of the URL and return the Output from the Request
 * 
 * @param string $url
 * @return string
 */
function black_http_request($url){
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL             ,$url);
    curl_setopt($ch, CURLOPT_HEADER          ,0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER  ,TRUE);
    $output = curl_exec($ch);
    return $output;
    
}
/**
 * Get the distance Between Places
 * 
 * Get the Distance Between the "To" and "From"  Based on the Google Distance API
 * @param string $to
 * @param string $from
 * @param array  $args
 * @return distanceobject
 */
function black_get_distance_between($to,$from,$args=array()){
    
    $args=array_merge($args,array(
        'output'         => 'json',
        'driving_mode'   => 'driving'
    ));
    
    $url  = 'http://maps.googleapis.com/maps/';
    $url .= BLACK_GOOGLE_API;
    $url .= '/distancematrix/' . $args['output'];
    $url .= '?destinations=' . urlencode($to);
    $url .= '&origins=' . urlencode($from);
    $url .= '&mode=' . $args['driving_mode'];
    $url .= '&sensor=false';
    
    return black_http_request($url);
}
