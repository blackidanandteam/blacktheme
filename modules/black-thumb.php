<?php
/**
 * @package Black
 * @subpackage wp_thumb
 */

/**
 * Get the Runtime Image thumbnil from the phpThumb
 * 
 * @param string $src
 * @param array  $args
 * @return type
 */
function black_get_thumburl($src,$args){
    
    $data=http_build_query(array_merge(array('src'=>$src),$args));
    
    return get_bloginfo('template_directory') . "/lib/phpthumb/phpThumb.php?" . $data ;
}