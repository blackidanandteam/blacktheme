<?php
/**
 * @package Black
 * @subpackage General
 */

/**
 * Redirect Un Authenticated user to Specific or Home Page Url
 * using PHP Header function 
 * 
 * Note: this function must be used Before any Output Starts As using Php Header
 *       function for Redirection 
 * 
 * @param type $url 
 */
function unauth_user($url=null,$isajax = FALSE){
    $redirect_url = (is_null($url))?esc_url(home_url('')):$url;
    if(!$isajax){
        header("location:". $redirect_url);
    }else{
        ?>
    <script type="text/javascript">
        window.location='<?PHP echo $redirect_url; ?>';
    </script>
        <?PHP
    }
    die("");
}

/**
 * Exe Div for Various Ajax Script in the Theme
 */
add_action('wp_footer',function(){
    ?>
<div id="exe" style="visibility:hidden;display:none;"></div>
    <?PHP
});

/**
 * Site wide Javascript Variables
 */
add_action('wp_head',function(){
?>
<script type="text/javascript">
black = {
    requesthandler:'<?PHP echo esc_url(home_url('requesthandler')); ?>'
};
</script>
<?PHP    
});

function black_set_message($args){
    
    foreach($args as $key=>$value){
        setcookie($key,$value,0,'/');
    }
    header("location:". esc_url(home_url('message')));
}
