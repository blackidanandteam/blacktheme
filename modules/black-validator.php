<?php

abstract class BLACK_VALIDATOR {
    
    public $error_flag;
    public $error_message;
    public $fields;
    
    function __construct() {
        $this->error_flag       =  FALSE;
        $this->error_message    =  array();
    }
    /**
     * Add the Error Message in A Error List
     * 
     * @param string $key
     * @param string $msg
     * @param type $args
     */
    function add($key,$msg,$args = array()){
        $this->error_flag          = TRUE; 
        $this->error_message[$key] = $msg;
    }
    /**
     * Remove the Error From the Error Message List
     * 
     * @param type $key
     */
    function remove($key){
        if(key_exists($key,$this->error_message))
            unset ($this->error_message[$key]);
        if(count($this->error_message)==0)
            $this->error_flag = FALSE;
        
    }
    
    /**
     * Get the List of errors After Applying the Validation Process
     * 
     * @param string $format
     * @return mixed
     */
    function get_errors($format = 'default'){
        switch($format){
            case 'default':
            default:
                return $this->error_message;
                break;
            case 'json';
                foreach($this->error_message as $key=>$value){
                    $errors[] = array('key'=>$key,'value'=>$value) ;
                }
                return json_encode($errors);
                break;
        }
    }
    
    function is_having_error(){
        return $this->error_flag;
    }
    
    function validate_field($field_key){
        if(method_exists($this,'validate_' . $field_key )){
            $method = 'validate_' . $field_key;
            $this->$method();
        }else{
            $this->__get($field_key);
        }
    }
    
    /**
     * 
     * @param type $name
     * @return type
     */
    function __get($name) {
        return;
    }
    
    abstract function validate();
}